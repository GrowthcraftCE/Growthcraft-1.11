package growthcraft.cellar.proxy;

public class CommonProxy {

    public void init() {}

    public void registerRenders() {}

    public void registerTileEntities() {}

    public void registerModelBakeryVariants() { }
}
