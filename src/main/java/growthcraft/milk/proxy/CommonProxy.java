package growthcraft.milk.proxy;

public class CommonProxy {

    public void init() { }
    public void registerRenders() { }
    public void registerModelBakeryVariants() { }
    public void registerSpecialRenders() { }
    public void registerTitleEntities() { }

}
